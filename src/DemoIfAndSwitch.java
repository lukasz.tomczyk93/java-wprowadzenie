import java.util.Scanner;

public class DemoIfAndSwitch {

    public static void main(String[] args) {

        var myScanner = new Scanner(System.in);

        System.out.println("Wprowadź wartność typu int");

        int providedNumber = myScanner.nextInt();

//        testIf(providedNumber);
//        testSwitch(providedNumber);
    }

    private static void testSwitch(int providedNumber) {
        switch (providedNumber) {
            case 10:
                System.out.println("Bingo! Trafiłeś w 10!");
                break;
            case 20:
                System.out.println("Bingo! Trafiłeś w 20!");
                break;
            case 30:
                System.out.println("Trafiłeś w 30 - nie przerywam switcha");
            case 40:
                System.out.println("Trafiłeś w grupę 30,40");
                break;
            default:
                System.out.println("Nie trafiłeś w nic :(");
        }
    }

    private static void testIf(int providedNumber) {
        if(providedNumber > 10) {
            System.out.println("Wartość większa od 10");

            if (providedNumber == 20) {
                System.out.println("Bingo! Trafiłeś w 20!");
            }

        } else if (providedNumber == 10) {
            System.out.println("Bingo! Trafiłeś w 10!");
        }
        else {
            System.out.println("Wartość nie jest większa od 10");
        }
    }
}
