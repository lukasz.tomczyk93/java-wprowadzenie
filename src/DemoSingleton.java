class DemoSingleton {
    public static void main(String[] args) {
        Session instance_v1 = Session.INSTANCE;
        Session instance_v2 = Session.INSTANCE;
        Session instance_v3 = Session.INSTANCE;
        System.out.println(instance_v1);
        System.out.println(instance_v2);
        System.out.println(instance_v3);
    }
}

class Session {

    static final Session INSTANCE = new Session();

    private Session() {
    }
}

enum SessionEnum {
    INSTANCE
}