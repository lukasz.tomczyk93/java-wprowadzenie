import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DemoLoops {

    public static void main(String[] args) {
//        testForiLoop();
//        testWhileLoop();
//        testDoWhileLoop();
//        testForEachLoop();
//        testStreamFilterAndForEach();
//        testDoWhilePlusScannerWithInvalidNumber();
    }

    private static void testDoWhilePlusScannerWithInvalidNumber() {
        Scanner scanner = new Scanner(System.in);
        int userInput = 0;
        do {
            System.out.println("Podaj liczbę:");
            try {
                userInput = Integer.parseInt(scanner.next());
            } catch (NumberFormatException e) {
                userInput = 0;
                System.out.println("Niepoprawne dane wejściowe!");
            }
        } while (!(userInput == 5));
    }

    private static void testStreamFilterAndForEach() {
        int[] numbers = {1, 2, 5, 4, 3, 6, 7, 565, 3};
        Arrays.stream(numbers)
                .filter(number -> number % 2 == 0)
                .forEach(number -> System.out.println(number)); //to samo co: .forEach(System.out::println);
    }

    private static void testForEachLoop() {
        int[] numbers = {1, 2, 5, 4, 3, 6, 7, 565, 3};
        for (int number : numbers) {
            System.out.println(number);
        }
    }

    private static void testDoWhileLoop() {
        int j = 0;
        do {
            System.out.println(j);
            j += 2;
        } while (j < 10);
    }

    private static void testWhileLoop() {
        int i = 1;
        while (i <= 10) {
            System.out.println(i);
            i++;
        }
    }

    private static void testForiLoop() {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
    }
}
