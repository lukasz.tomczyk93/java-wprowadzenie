import java.text.MessageFormat;
import java.util.Scanner;

public class DemoObjectsWithUser {
    public static void main(String[] args) {

        var scanner = new Scanner(System.in);
        System.out.println("Enter first name: ");
        var firstName = scanner.next();

        System.out.println("Enter last name: ");
        var lastName = scanner.next();

        System.out.println("Enter age: ");
        var age = scanner.nextInt();

        var user = User.of(firstName, lastName, age);

        user.printDetails();
    }
}

class User {

    private final String firstName;
    private final String lastName;
    private final int age;

    private User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    static User of(String firstName, String lastName, int age) {
        return new User(firstName, lastName, age);
    }

    void printDetails() {
        System.out.println("User details:");
        System.out.println(String.format("First name: %s", firstName));
        System.out.println(MessageFormat.format("Last name: {0}", lastName));
        System.out.println("Age: " + age);
    }
}