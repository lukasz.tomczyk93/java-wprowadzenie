class DemoStatic {
    public static void main(String[] args) {
        String helloText = StringFactory.createHelloText();
        String byByText = StringFactory.createByByText();
        int maxNumberOfThreads = StringFactory.MAX_NUMBER_OF_THREADS;
        StringFactory.StringFactoryHelper stringFactoryHelper = new StringFactory.StringFactoryHelper();
    }
}

class StringFactory {

    static final int MAX_NUMBER_OF_THREADS = 20;

    static String createHelloText() {
        return "Hello world!";
    }

    static String createByByText() {
        return "Bye bye!";
    }

    static class StringFactoryHelper {

    }
}