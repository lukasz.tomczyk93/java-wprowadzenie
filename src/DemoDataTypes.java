import java.math.BigInteger;

class DemoDataTypes {

    public static void main(String[] args) {
        char znak = 1 + 2;
        char inny_znak = 'c';
        byte bajt = 10;
        short liczbaShort = 10;
        int liczbaCalkowita = 100;
        long liczbaLong = Long.MAX_VALUE;
        float liczbaZPrzecinkiemDoJednegoMiejsca = 12.1F;
        double liczbaZprzecinkiem = 12.23;
        boolean typLogiczny = false;

        String text = "Text";
        String inny_string = "Text";
        BigInteger bigInt = BigInteger.valueOf(1000000000000000L).multiply(BigInteger.valueOf(90000000000000000L));

        Integer testDomysla = 10;
        Boolean aBoolean = new Boolean(true);

        MyNewType myNewType = new MyNewType();
    }
}

class MyNewType {

    public static void main(String... args) {
        System.out.println(args[0] + "," + args[1] + "," + args[2]);
    }
}
